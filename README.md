#Test de compétence Développeur PHP SYMFONY 2.8
##1. Introduction
###1.1. Présentation
L’objectif de ce test est de faire un point sur les compétences du développeur dans un environnement familier. Le test est scindé en plusieurs parties : 
1. La compréhension de la problématique
2. Le traitement de la demande
3. L’explication de la réalisation

###1.2. Installation
Prérequis :
* git
* docker
* docker-compose

Compétences requises :
* PHP
* Composer
* SQL
* Docker

Pour installer les containers :
    
    docker-compose -f infrastructure/docker-compose.yml up --build -d

Pour entrer dans le container fpm :

    docker exec -it infrastructure_fpm_1 bash
    
!!! Si vous êtes sous linux, pensez à faire en sorte de l'UID de votre user corresponde !!!

    cf: infrastructure/services/fpm/Dockerfile:33

##2. Cahier des charges
Vous devrez pour ce projet réaliser un formulaire d’ajout d’articles de blog, ainsi que l’affichage de ceux-ci. Ils devront être stockés dans une base de données Mysql. 
Tous les visiteurs du site peuvent ajouter un article dans le blog. 
La page d’accueil doit contenir les 3 derniers articles postés, ainsi qu’un formulaire d’ajout d’article.
Un simple clic sur le titre ou l’image d’un article fait apparaître l’article dans une nouvelle page.
###2.1. Création d’un article
Sur la page d’accueil par défaut l’utilisateur se voit proposer un formulaire d’ajout d’articles. Ce formulaire doit contenir les champs suivant :
* Titre (max 60 caractères)*
* Description (texte enrichit) 
* Image (jpg, gif, png et max 500 ko) 
* Nom (max 50 caractères)*
* Prénom (max 50 caractères)*
* Ville*
* Email*

Le formulaire doit être sécurisé. 
Les champs suivis d’un * sont obligatoires. 
Chaque champ doit-être vérifié à la soumis du formulaire sur le serveur.
###2.2. Affichage d’un article
L’affichage de la notice doit faire apparaitre tous les champs.
###2.3. Point supplémentaires
Le site doit être responsive. 


##3. Supports
###3.1. Schéma
Voici ci-dessous l’ébauche du schéma de base de la BDD. A vous de l’ajuster en fonction des besoins

![](doc/img/schema-bdd.png "Schema BDD")

###3.2. Maquette
En option : Voici les maquettes à intégrer. 
####3.2.1. Page d’accueil sans formulaire

![](doc/img/maquette-1.png "Maquette 1")

####3.2.2. Détail d’un article

![](doc/img/maquette-2.png "Maquette 2")
